﻿<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main extends CI_Controller {

	
	function __construct()
    {
        // this is your constructor
        parent::__construct();
		$this->load->database();
        $this->load->helper('form');
        $this->load->helper('url');
		
    }
	
	public function index()
	{
		$this->load->view('main');	
	
	
	}
	
	
	public function error()
	{		
	$this->load->view('content');	
	}
	
	
	public function search()
	{		
		

		
		
		
			
			$search_words = trim(urldecode($this->uri->segment(2))) ;
			
			$search_words =  str_replace(" ", "%", $search_words);

			$query = $this->db->query("SELECT * FROM content WHERE keywords like ? order by id desc limit 1;",array('%'.$search_words.'%'));	

				

			foreach($query->result_array() as $row)
			{
			
				redirect($row["url"].'/search/'.urldecode($this->uri->segment(2)), 'location');
				
			}
		
		
			echo "aradığınız kriterlere uygun sonuç bulunamamıştır.";
		
		
	
		
	}
	
	
	
	
	public function content()
	{
	
			if($this->uri->segment(2) == null)
			{
				$query = $this->db->query("SELECT * FROM osokak.content order by id desc limit 1");	
	
				foreach($query->result_array() as $row)
				{
						redirect($row["id"]."/".$row["url"] , 'location');
					
					//redirect($row["url"] , 'location');
					
				}
			}
			else
			{
	 
	
				$query = $this->db->query("SELECT * FROM osokak.content where id = ?",array($this->uri->segment(1)));	

				$result = $query->result();
				 
				if($result == null)
				{
						redirect("http://www.osokak.com/error" , 'location');
					return;
				}
				
				$this->load->view('content',$result[0]);	
			
			}
	
		
	}
	
	
	
	
	
	
	public function get_new_data()
	{
		
		if(is_numeric($this->input->get('id') != 1))
		{
		echo "error";
		return;
		}
	
		
		 
		 if($this->input->get('search', TRUE) == "null")
		 {	
		 
		 
				
				$query = $this->db->query("SELECT id, content_html,page_title , CONCAT('/', id, '/', url) AS url   FROM content WHERE  id < ?  order by id desc limit 1;


",array($this->input->get('id')));	
		 
		 
		 
				$result = $query->result_array();
				
					
				$json=json_encode($result[0]);
				$this->output->set_content_type('application/json')->set_output($json);
		 }
		 else
		 {
		 
			
		 
				
				$search_words = trim(urldecode($this->input->get('search', TRUE))) ;
			
				$search_words =  str_replace(" ", "%", $search_words);

				
				$query = $this->db->query("SELECT * FROM content WHERE keywords like ? and id < ? order by id desc limit 1;",array('%'.$search_words.'%',(int)$this->input->get('id')));		
		 
		 
				$result = $query->result_array();
				
					
				$json=json_encode($result[0]);
				$this->output->set_content_type('application/json')->set_output($json);
				
		 }
		
		
		
	
	
	
	}
	
	
	
	
	
	
	
	
	
	public function admin()
	{
		$this->load->library('session');
		$this->load->library('form_validation');
				
		$this->form_validation->set_rules('txtContent', 'txtContent', 'required');
		$this->form_validation->set_rules('txtTitle', 'txtTitle', 'required');
		$this->form_validation->set_rules('txtUrl', 'txtUrl', 'required');
		
		if(!$this->session->userdata('user'))
		{
			redirect("/main/login","refresh");
		}
	
		
		
		
		
		if($_POST && $this->form_validation->run() === TRUE  &&   $this->uri->segment(2) != ''  && $this->uri->segment(2) != null )
		{
				
			
			$id = $this->uri->segment(2);
			$title = $this->input->post('txtTitle');
			$content = $this->input->post('txtContent');
			$url = $this->input->post('txtUrl');
			
			$data = array(
			   'page_title' => $title,
			   'content_html' => $content,
			   'url' => $url
			);
			
			$this->db->where('id', $id);
			$this->db->query($this->db->update('content', $data));

				redirect("/admin","refresh");
		
		}
		
		
		if( !$_POST && $this->uri->segment(2)    )
		{
				
				$query = $this->db->query("SELECT * FROM osokak.content where id = ?",array( $this->uri->segment(2)));	

				$result = $query->result();
				  
				if($result == null)
				{
					echo "bir şeyler ters gitmiş olabilir. ya da o id yok...";
					return;
				}
				
				$this->load->view('admin',$result[0]);	
				
		}
		
		
		
		if( $_POST && $this->form_validation->run() === TRUE  &&  !$this->uri->segment(2) ) 
				{
				
					
					$title = $this->input->post('txtTitle');
					$content = $this->input->post('txtContent');
					$url = $this->input->post('txtUrl');
					
					$data = array(
					   'page_title' => $title,
					   'content_html' => $content,
					   'url' => $url
					);

					$this->db->query($this->db->insert('content', $data));

					//echo $this->db->insert_id();
					redirect("/admin","refresh");

				}
		
		
		
		
		if( !$_POST && !$this->uri->segment(2)    )
		{
			
			$this->load->view('admin');	
		}
			
		
	}
	
	function login()
	{
	
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('email', 'email', 'required');
		$this->form_validation->set_rules('password', 'password', 'required');

		
		
		if (  $_POST &&  $this->form_validation->run() == true &&  $this->input->post('email' ) == "admin@gmail.com" &&  $this->input->post('password') == "xxx")
		{
		
			$this->load->library('session');
		
			$this->session->unset_userdata('user');
			$this->session->set_userdata("user","loggedin");
			redirect('/admin', 'refresh');
			return;
			//$remember = (bool) $this->input->post('remember');
			
		}

	
		$this->load->view("login");
	
	}
	
	
	
	

	
}
