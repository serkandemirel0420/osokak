<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/ 

$route['default_controller'] = "main/content";
$route['404_override'] = '';

//$route['main/main_dev'] = "main/main_dev";

//$route['(:num)/(:any)'] = "main/karikatur"; 

//$route[ '(:num)'.'-'.'(:num)'.'-'.'(:num)/(:num)/(:any)'] = "main/main_dev"; 

//$route[ '(:num)/(:any)/x'] = "main_dev/index"; 


$route['(:num)/(:any)/search/(:any)'] = "main/content"; 

$route['(:num)/(:any)'] = "main/content"; 

$route['search/(:any)'] = "main/search";  

$route['content'] = "main/content";

$route['admin'] = "main/admin";
$route['admin/(:num)'] = "main/admin";

//bu alt çizgi ve düz çizgiyi de ekliyo
//$route['([A-Za-z\-\_]+)'] = "main/type"; 

//$route['type/(:any)'] = "main/type";  


//$route['(:num)/(:any)/(:any)'] = "main"; 

// $route[ '(:num)/(:any)'] = "main/index"; 
// $route[ 'son_dakika/(:num)'] = "main/son_dakika"; 
// $route[ 'son_dakika'] = "main/son_dakika"; 
// $route['x/(:num)/(:any)'] = "main/new_index"; 




//$route[ 'karikatur/(:num)'.'-'.'(:num)'.'-'.'(:num)'] = "main/main_dev";


/* End of file routes.php */
/* Location: ./application/config/routes.php */